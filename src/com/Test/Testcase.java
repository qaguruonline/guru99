package com.Test;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Page.homePage;
import com.Page.loginPage;

public class Testcase {

	WebDriver driver;

	loginPage objLogin;

	homePage objHomePage;

	@BeforeTest

	public void setup() {

		driver = new ChromeDriver();
		System.setProperty("webdriver.chrome.driver", "/Users/qatester/Documents/seleniumdrivers/chromedriver");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://demo.guru99.com/V4/");

	}

	/**
	 * 
	 * This test case will login in http://demo.guru99.com/V4/
	 * 
	 * Verify login page title as guru99 bank
	 * 
	 * Login to application
	 * 
	 * Verify the home page using Dashboard message
	 * 
	 */

	@Test
	public void test_Home_Page_Appear_Correct() {

		// Create Login Page object

		objLogin = new loginPage(driver);

		// Verify login page title

		String loginPageTitle = objLogin.getLoginTitle();

		Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));

		// login to application

		objLogin.loginToGuru99("mgr123", "mgr!23");

		// go the next page

		objHomePage = new homePage(driver);

		// Verify home page
		Assert.assertTrue(objHomePage.getHomePageDashboardUserName().toLowerCase().contains("manger id : mgr123"));

	}

	@Test
	public void testMenuOptions() {
		objHomePage = new homePage(driver);
		System.out.println(objHomePage.getMenuOptionSele());
		Assert.assertTrue(objHomePage.getMenuOptionSele().contains("Selenium"));

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}