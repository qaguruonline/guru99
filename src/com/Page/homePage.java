package com.Page;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

public class homePage {

	WebDriver driver;

	By homePageUserName = By.xpath("//table//tr[@class='heading3']");
	By menuOptionSelenium = By.xpath("//a[@class='dropdown-toggle']");

	public homePage(WebDriver driver) {

		this.driver = driver;

	}

	// Get the User name from Home Page

	public String getHomePageDashboardUserName() {

		return driver.findElement(homePageUserName).getText();

	}

	public String getMenuOptionSele() {
		return driver.findElement(menuOptionSelenium).getText();

	}

}